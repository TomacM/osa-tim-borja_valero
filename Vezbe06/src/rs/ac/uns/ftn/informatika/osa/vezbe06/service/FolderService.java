package rs.ac.uns.ftn.informatika.osa.vezbe06.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder;
import rs.ac.uns.ftn.informatika.osa.vezbe06.repository.FolderRepository;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.FolderServiceInterface;

@Service
public class FolderService implements FolderServiceInterface{

	@Autowired
	FolderRepository folderRepository;

	@Override
	public List<Folder> findByParent(Folder parent) {
		return folderRepository.findByParent(parent);
	}


	@Override
	public Optional<Folder> findOne(Integer folderId) {
		
		return folderRepository.findById(folderId);
	}

	@Override
	public Folder save(Folder folder) {
		return folderRepository.save(folder);
	}

	@Override
	public void remove(Integer id) {
		folderRepository.deleteById(id);
		
	}

	@Override
	public Folder findByName(String name) {
		return folderRepository.findByName(name);
	}

	@Override
	public Folder findByNameAndAccount(String name, Account account) {
		return folderRepository.findByNameAndAccount(name, account);
	}


	@Override
	public List<Folder> findAll() {
		return folderRepository.findAll();
	}
	
}
