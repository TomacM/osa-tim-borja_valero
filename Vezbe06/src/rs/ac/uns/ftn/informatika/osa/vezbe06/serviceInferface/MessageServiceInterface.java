package rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface;

import java.util.List;
import java.util.Optional;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;

public interface MessageServiceInterface {

	Optional<Email> findOne(Integer emailId);
	
	List<Email> findAll();
	
	Email save(Email message);
	
	void remove(Integer id);
	
	List<Email> findBySubject(String subject);
	
}
