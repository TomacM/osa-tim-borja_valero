package rs.ac.uns.ftn.informatika.osa.vezbe06.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Tag;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.User;

public interface TagRepository extends JpaRepository<Tag, Integer> {
	List<Tag> findByUser(User user);
	
	Tag findByNameAndUser(String name, User user);
}


