package rs.ac.uns.ftn.informatika.osa.vezbe06.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {

}

