package rs.ac.uns.ftn.informatika.osa.vezbe06.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.EmailDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.FolderDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.AccountServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.FolderServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.MessageServiceInterface;

@RestController
@RequestMapping(value="/folders")
public class FolderController {
	@Autowired
	FolderServiceInterface folderService;
	
	@Autowired
	AccountServiceInterface accountService;
	
	@Autowired
	MessageServiceInterface messageService;
	
	
	@GetMapping
	public ResponseEntity<List<FolderDTO>> getAllFolders(){
		List<Folder> folders = folderService.findAll();
		if (folders == null) {
			return new ResponseEntity<List<FolderDTO>>(HttpStatus.NOT_FOUND);
		}
		List<FolderDTO> folderDTOs = new ArrayList<>();
		for (Folder folder : folders) {
			folderDTOs.add(new FolderDTO(folder));
		}
		return new ResponseEntity<List<FolderDTO>>(folderDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/account/{username}")
	public ResponseEntity<List<FolderDTO>> getAllFoldersFromUser(@PathVariable("username") String username){
		Account account = accountService.findByUsername(username);
		if (account == null) {
			return new ResponseEntity<List<FolderDTO>>(HttpStatus.BAD_REQUEST);
		}
		List<FolderDTO> folderDTOs = new ArrayList<>();
		for (Folder itFolder : account.getAccountFolder()) {
			folderDTOs.add(new FolderDTO(itFolder));
		}
		return new ResponseEntity<List<FolderDTO>>(folderDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<FolderDTO> getFolder(@PathVariable("id") Integer id){
		Optional<Folder> folderOptional = folderService.findOne(id);
		Folder folder = folderOptional.get();
		if(folder == null){
			return new ResponseEntity<FolderDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<FolderDTO>(new FolderDTO(folder), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteFolder(@PathVariable("id") Integer id){
		System.out.println("BRISI.....");
		Optional<Folder> folderOptional = folderService.findOne(id);
		Folder folder = folderOptional.get();
		if (folder != null){
			folderService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value = "/{username}",consumes="application/json")
	public ResponseEntity<FolderDTO> addNewFolder(@RequestBody FolderDTO folderDTO,
			@PathVariable("username") String username){
		System.out.println("Dodavanje foldera");
		Account account = accountService.findByUsername(username);
		
		if (account == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		Folder folder = new Folder();
		
		folder.setName(folderDTO.getName());
		folder.setFolderRule(null);
		
		account.add(folder);
		
		folder = folderService.save(folder);
		
		return new ResponseEntity<FolderDTO>(new FolderDTO(folder),HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<FolderDTO> updateFolder(@RequestBody FolderDTO folderDTO, @PathVariable("id") Integer id){
		//a category must exist
		System.out.println("ajde");
		Optional<Folder> folderOptional = folderService.findOne(id);
		Folder folder = folderOptional.get();
		if (folder == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		
		folder.setName(folderDTO.getName());
		Rule rule;
		if (folder.getFolderRule().iterator().hasNext()) {
			
			rule = folder.getFolderRule().iterator().next();
		} else {
			rule = new Rule();
		}
		
		rule.setOperation(folderDTO.getRule().getOperation());
		rule.setCondition(folderDTO.getRule().getCondition());
		
		folder.add(rule);
		
		folder = folderService.save(folder);
		
		return new ResponseEntity<FolderDTO>(new FolderDTO(folder), HttpStatus.OK);	
	}
	

	
	
}

