package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule;

public class FolderDTO implements Serializable {
    private Integer id;
    private String name;
    private List<FolderDTO> folders;
    private RuleDTO rule;
    private List<EmailDTO> messages;

	public FolderDTO() {
	}
	


    public FolderDTO(Integer id, String name, List<FolderDTO> folders, RuleDTO rule, List<EmailDTO> messages){
        this.id = id;
        this.name = name;
        this.folders = folders;
        this.rule = rule;
        this.messages = messages;
    }

    public FolderDTO (Folder folder) {
    	Rule rule= null;
    	if (folder.getFolderRule() != null) {
    		System.out.println(folder.getFolderRule().size());
    		Iterator<Rule> iterator = folder.getFolderRule().iterator();
    		while(iterator.hasNext()) {
    			rule = iterator.next();
    		}
        	
		} 
    	List<FolderDTO> folderDTOs = new ArrayList<>();
    	if (folder.getChildrenFolder() != null && !folder.getChildrenFolder().isEmpty()) {
        	for (Folder itFolder : folder.getChildrenFolder()) {
        		List<EmailDTO> messageDTOs2 = new ArrayList<>();
            	for (Email itMessage: folder.getMessages()) {
        			messageDTOs2.add(new EmailDTO(itMessage));
        		}
        		Rule itRule = null;
        		Iterator<Rule> iterator = itFolder.getFolderRule().iterator();
        		while(iterator.hasNext()) {
        			itRule = iterator.next();
        		}
        		RuleDTO itRuleDTO =(itRule == null)?null: new RuleDTO(itRule);
        		
    			FolderDTO itFolderDTO = new FolderDTO(itFolder.getId(), itFolder.getName(),
    					new ArrayList<>(), itRuleDTO, messageDTOs2);
    			folderDTOs.add(itFolderDTO);
    		}
		}

    	//poruke
    	List<EmailDTO> messageDTOs = new ArrayList<>();
    	if (folder.getMessages() != null) {
        	for (Email itMessage: folder.getMessages()) {
    			messageDTOs.add(new EmailDTO(itMessage));
    		}
		}

    	//
    	
    	RuleDTO ruleDTO = (rule == null)?null: new RuleDTO(rule);

    	this.setId(folder.getId());
    	this.setName(folder.getName());
    	this.setFolders(folderDTOs);
    	this.setRule(ruleDTO);
    	this.setMessages(messageDTOs);
    }


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public List<FolderDTO> getFolders() {
		return folders;
	}



	public void setFolders(List<FolderDTO> folders) {
		this.folders = folders;
	}



	public RuleDTO getRule() {
		return rule;
	}



	public void setRule(RuleDTO rule) {
		this.rule = rule;
	}



	public List<EmailDTO> getMessages() {
		return messages;
	}



	public void setMessages(List<EmailDTO> messages) {
		this.messages = messages;
	}

	

}
