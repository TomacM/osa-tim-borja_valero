package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import java.io.Serializable;
import java.util.Base64;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Attachment;

public class AttachmentDTO implements Serializable {

    private Integer id;
    private String data;
    private String mime_type;
    private String name;

	public AttachmentDTO() {
		super();
	}


	   public AttachmentDTO(Integer id, String data, String mime_type, String name) {
		super();
		this.id = id;
		this.data = data;
		this.mime_type = mime_type;
		this.name = name;
	}


	public AttachmentDTO(Attachment attachment) {

	    	if (attachment.getId() != null) {
	    		this.id = attachment.getId();
			}
	    	
	    	this.data = attachment.getData();
	    	this.mime_type= attachment.getType();
	    	this.name = attachment.getName();
	    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMime_type() {
		return mime_type;
	}

	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
