package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import java.io.Serializable;
import java.util.ArrayList;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Tag;

public class TagDTO implements Serializable {
    private Integer id;
    private String name;
    private UserDTO user;

    public TagDTO() {
		super();
	}

    
	public TagDTO(Integer id, String name, UserDTO user) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
	}
    public TagDTO(Tag tag) {

    	this.setId(tag.getId());
    	this.setName(tag.getName());

    }
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	public UserDTO getUser() {
		return user;
	}


	public void setUser(UserDTO user) {
		this.user = user;
	} 
	

}
