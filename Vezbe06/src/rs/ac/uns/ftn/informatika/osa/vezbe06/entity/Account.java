package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "accounts")
public class Account implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "account_smtp_address", unique = false, nullable = false, length=250)
	private String smtpAddress;

	@Column(name = "account_smtp_port", unique = false, nullable = false)
	private Integer smtpPort;

	@Column(name = "account_in_server_type", unique = false, nullable = false)
	private Integer inServerType;

	@Column(name = "account_in_server_address", unique = false, nullable = false, length=250)
	private String inServerAddress;
	
	@Column(name = "account_pop3_imap", unique=false, nullable=false, length=10)
	private String pop3Imap;
	
	@Column(name = "account_in_server_port", unique = false, nullable = false)
	private Integer inServerPort;

	@Column(name = "account_username", unique = true, nullable = false, length=40)
	private String username;

	@Column(name = "account_password", unique = false, nullable = false, length=20)
	private String password;

	@Column(name = "account_display_name", unique = true, nullable = false, length=30)
	private String displayName;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "account")
	private Set<Folder> accountFolder=new HashSet<Folder>();
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "account")
	private Set<Email> accountEmail=new HashSet<Email>();
	
	public Account(Integer id, String smtpAddress, Integer smtpPort, Integer inServerType, String inServerAddress,
			Integer inServerPort,String pop3Imap, String username, String password, String displayName, User user, Set<Folder> accountFolder, Set<Email> accountEmail) {

		this.id = id;

		this.smtpAddress = smtpAddress;

		this.smtpPort = smtpPort;
		
		this.pop3Imap = pop3Imap;

		this.inServerType = inServerType;

		this.inServerAddress = inServerAddress;

		this.inServerPort = inServerPort;

		this.username = username;

		this.password = password;

		this.displayName = displayName;
		
		this.user = user;
				
		this.accountFolder = accountFolder;
		
		this.accountEmail = accountEmail;

	}

	public Account() {

	}
	
	public void add(Email message) {
		if (message.getAccount() != null) {
			message.getAccount().getAccountEmail().remove(message);
		}
		message.setAccount(this);
		getAccountEmail().add(message);
	}
	
	public void add(Folder folder) {
		if (folder.getAccount() != null) {
			folder.getAccount().getAccountFolder().remove(folder);
		}
		folder.setAccount(this);
		getAccountFolder().add(folder);
	}
	
	public void remove(Email message) {
		message.setAccount(null);
		getAccountEmail().remove(message);
	}
	
	public void remove(Folder folder) {
		folder.setAccount(null);
		getAccountFolder().remove(folder);
	}

	public Integer getId() {

		return id;

	}

	public void setId(Integer id) {

		this.id = id;

	}

	public String getSmtpAddress() {

		return smtpAddress;

	}

	public void setSmtpAddress(String smtpAddress) {

		this.smtpAddress = smtpAddress;

	}

	public Integer getSmtpPort() {

		return smtpPort;

	}

	public void setSmtpPort(Integer smtpPort) {

		this.smtpPort = smtpPort;

	}

	public Integer getInServerType() {

		return inServerType;

	}

	public void setInServerType(Integer inServerType) {

		this.inServerType = inServerType;

	}

	public String getInServerAddress() {

		return inServerAddress;

	}

	public void setInServerAddress(String inServerAddress) {

		this.inServerAddress = inServerAddress;

	}

	public Integer getInServerPort() {

		return inServerPort;

	}

	public void setInServerPort(Integer inServerPort) {

		this.inServerPort = inServerPort;

	}

	public String getUsername() {

		return username;

	}

	public void setUsername(String username) {

		this.username = username;

	}

	public String getPassword() {

		return password;

	}

	public void setPassword(String password) {

		this.password = password;

	}

	public String getDisplayName() {

		return displayName;

	}

	public void setDisplayName(String displayName) {

		this.displayName = displayName;

	}
	
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Folder> getAccountFolder() {
		return accountFolder;
	}

	public void setAccountFolder(Set<Folder> accountFolder) {
		this.accountFolder = accountFolder;
	}

	
	
	public Set<Email> getAccountEmail() {
		return accountEmail;
	}

	public void setAccountEmail(Set<Email> accountEmail) {
		this.accountEmail = accountEmail;
	}
	

	public String getPop3Imap() {
		return pop3Imap;
	}

	public void setPop3Imap(String pop3Imap) {
		this.pop3Imap = pop3Imap;
	}

	@Override

	public String toString() {

		return "Account{" +

				"id=" + id +

				", smtpAddress='" + smtpAddress + '\'' +

				", smtpPort=" + smtpPort +

				", inServerType=" + inServerType +

				", inServerAddress='" + inServerAddress + '\'' +

				", inServerPort=" + inServerPort +
				
				 ", pop3Imap=" + pop3Imap +

				", username='" + username + '\'' +

				", password='" + password + '\'' +

				", displayName='" + displayName + '\'' +
				
				", user='" + user + '\'' +

	            ", accountFolder=" + accountFolder + '\'' +
	            
	            ", accountEmail=" + accountEmail +
				'}';

	}

}
