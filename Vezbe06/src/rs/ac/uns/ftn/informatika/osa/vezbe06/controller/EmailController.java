package rs.ac.uns.ftn.informatika.osa.vezbe06.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
//import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.AttachmentDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.EmailDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.TagDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Attachment;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule.Condition;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule.Operation;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Tag;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.User;
import rs.ac.uns.ftn.informatika.osa.vezbe06.mailApi.MailGetter;
import rs.ac.uns.ftn.informatika.osa.vezbe06.mailApi.MailSender;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.AccountServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.FolderServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.MessageServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.UserServiceInterface;

@RestController
@RequestMapping(value="messages")
public class EmailController {
	@Autowired
	MessageServiceInterface messageService;
	
	@Autowired
	AccountServiceInterface accountService;
	
	@Autowired
	FolderServiceInterface folderService;
	
	@Autowired
    UserServiceInterface userService;
	
	@GetMapping
	public ResponseEntity<List<EmailDTO>> getAllMessages(){
		List<Email> messages = messageService.findAll();
		if (messages == null) {
			return new ResponseEntity<List<EmailDTO>>(HttpStatus.NOT_FOUND);
		}
		List<EmailDTO> messageDTOs = new ArrayList<>();
		for (Email message : messages) {
			messageDTOs.add(new EmailDTO(message));
		}
		return new ResponseEntity<List<EmailDTO>>(messageDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/account/{username}")
	public ResponseEntity<List<EmailDTO>> getAllMessagesFromUser(@PathVariable("username") String username){
		//MailGetter mailGetter = new MailGetter(messageService, folderService);
		Account account = accountService.findByUsername(username);
		if (account == null) {
			return new ResponseEntity<List<EmailDTO>>(HttpStatus.BAD_REQUEST);
		}
		//Date maxDate = account.getAccountEmail().stream().map(Email::getDateTime).max(Date::compareTo).get();
		//mailGetter.loadEmails(account, maxDate);
		List<EmailDTO> messageDTOs = new ArrayList<>();
		for (Email itMessage : account.getAccountEmail()) {
			if (itMessage.getFolder().getName().equals("Inbox")) {
				messageDTOs.add(new EmailDTO(itMessage));
			}

		}
		return new ResponseEntity<List<EmailDTO>>(messageDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/account/{username}/{name}")
	public ResponseEntity<List<EmailDTO>> getAllMessagesFolderUser(@PathVariable("username") String username, @PathVariable("name") String name){
		//MailGetter mailGetter = new MailGetter(messageService, folderService);
		Account account = accountService.findByUsername(username);
		Folder folder = folderService.findByNameAndAccount(name, account);
		if (account == null) {
			return new ResponseEntity<List<EmailDTO>>(HttpStatus.BAD_REQUEST);
		}
		//Date maxDate = account.getAccountEmail().stream().map(Email::getDateTime).max(Date::compareTo).get();
		//mailGetter.loadEmails(account, maxDate);
		List<EmailDTO> messageDTOs = new ArrayList<>();
		for (Email itMessage : account.getAccountEmail()) {
			if (itMessage.getFolder().getName().equals(folder.getName())) {
				messageDTOs.add(new EmailDTO(itMessage));
			}

		}
		messageDTOs.sort(Comparator.comparing(EmailDTO :: getDateTime).reversed());
		return new ResponseEntity<List<EmailDTO>>(messageDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/account/sortAsc/{username}")
	public ResponseEntity<List<EmailDTO>> getAllMessagesFromUserSortAsc(@PathVariable("username") String username){
		MailGetter mailGetter = new MailGetter(messageService, folderService);
		Account account = accountService.findByUsername(username);
		if (account == null) {
			return new ResponseEntity<List<EmailDTO>>(HttpStatus.BAD_REQUEST);
		}
		mailGetter.loadEmails(account);
		List<EmailDTO> messageDTOs = new ArrayList<>();
		for (Email itMessage : account.getAccountEmail()) {
			if (itMessage.getFolder().getName().equals("Inbox")) {
				messageDTOs.add(new EmailDTO(itMessage));
			}
		}
		
		messageDTOs.sort(Comparator.comparing(EmailDTO :: getDateTime));
		return new ResponseEntity<List<EmailDTO>>(messageDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/account/sortDes/{username}")
	public ResponseEntity<List<EmailDTO>> getAllMessagesFromUserSortDesc(@PathVariable("username") String username){
		MailGetter mailGetter = new MailGetter(messageService, folderService);
		Account account = accountService.findByUsername(username);
		if (account == null) {
			return new ResponseEntity<List<EmailDTO>>(HttpStatus.BAD_REQUEST);
		}
		mailGetter.loadEmails(account);
		List<EmailDTO> messageDTOs = new ArrayList<>();
		for (Email itMessage : account.getAccountEmail()) {
			if (itMessage.getFolder().getName().equals("Inbox")) {
				messageDTOs.add(new EmailDTO(itMessage));
			}
		}
		
		messageDTOs.sort(Comparator.comparing(EmailDTO :: getDateTime).reversed());
		return new ResponseEntity<List<EmailDTO>>(messageDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<EmailDTO> getMessage(@PathVariable("id") Integer id){
		Optional<Email> emailOptional = messageService.findOne(id);
		Email message = emailOptional.get();
		if(message == null){
			return new ResponseEntity<EmailDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<EmailDTO>(new EmailDTO(message), HttpStatus.OK);
	}
	
	
	@DeleteMapping(value="/delete/{id}")
	public ResponseEntity<Void> deleteMessage(@PathVariable("id") Integer id){
		Optional<Email> emailOptional = messageService.findOne(id);
		Email message = emailOptional.get();
		if (message != null){
			messageService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value = "/send/{username}", consumes="application/json")
	public ResponseEntity<EmailDTO> send(@RequestBody EmailDTO messageDTO, @PathVariable("username") String username){
		
		Account account = accountService.findByUsername(username);
		if (account == null) {
			return new ResponseEntity<EmailDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Folder folder = folderService.findByNameAndAccount("Sent", account);

		Email message = new Email();
		
		account.add(message);
		
		message.setDateTime(messageDTO.getDateTime());
		folder.add(message);
		message.setFrom(messageDTO.getFrom());
		message.setTo(messageDTO.getTo());
		message.setSubject(messageDTO.getSubject());
		message.setContent(messageDTO.getContent());

		message.setUnread(messageDTO.isProcitano());
		
		MailSender mailSender = new MailSender(messageService);
		mailSender.sendEmail(message);
		
		return new ResponseEntity<EmailDTO>(new EmailDTO(message) ,HttpStatus.OK);
	}
    @PutMapping(value = "/viewed/{id}")
    ResponseEntity<Void> getviewedMessages(@PathVariable("id") Integer id){
    	System.out.println("aj");
		Optional<Email> emailOptional = messageService.findOne(id);
		Email message = emailOptional.get();
    	if (message == null) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
    	message.setUnread(true);
    	messageService.save(message);
    	
    	return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
