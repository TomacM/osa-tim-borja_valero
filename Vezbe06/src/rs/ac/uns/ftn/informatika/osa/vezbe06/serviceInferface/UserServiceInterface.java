package rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface;

import java.util.List;
import java.util.Optional;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.User;

public interface UserServiceInterface {
	void save(User user);
    
	User findByUsername(String username);

	User findByUsernameAndPassword(String username, String password);
	
	Optional<User> findOne(Integer userId);
	
	List<User> findAll();
	
	
	
}
