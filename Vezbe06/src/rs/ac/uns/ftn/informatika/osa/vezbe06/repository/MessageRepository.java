package rs.ac.uns.ftn.informatika.osa.vezbe06.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;

import java.util.List;

public interface MessageRepository extends JpaRepository<Email, Integer> {
    
	List<Email> findBySubject(String subject);
	
}

