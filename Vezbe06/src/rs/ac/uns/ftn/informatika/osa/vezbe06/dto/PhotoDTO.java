package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import java.io.Serializable;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Photo;

public class PhotoDTO implements Serializable{
    private Integer id;
    private String path;
	
	public PhotoDTO() {
	}
    
	public PhotoDTO(Integer id, String path) {
	    this.id = id;
	    this.path = path;
	}
	public PhotoDTO(Photo photo) {
		this.setId(photo.getId());
		this.setPath(photo.getPath());
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

    
    
	
	

}
