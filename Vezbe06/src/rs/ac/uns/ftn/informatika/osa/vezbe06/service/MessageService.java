package rs.ac.uns.ftn.informatika.osa.vezbe06.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.repository.MessageRepository;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.MessageServiceInterface;

@Service
public class MessageService implements MessageServiceInterface{
	
	@Autowired
	MessageRepository messageRepository;



	@Override
	public Optional<Email> findOne(Integer messageId) {
		return messageRepository.findById(messageId);
	}

	@Override
	public List<Email> findAll() {
		return messageRepository.findAll();
	}

	@Override
	public Email save(Email message) {
		return messageRepository.save(message);
	}

	@Override
	public void remove(Integer id) {
		messageRepository.deleteById(id);
		
	}

	@Override
	public List<Email> findBySubject(String subject) {
		return messageRepository.findBySubject(subject);
	}

}
