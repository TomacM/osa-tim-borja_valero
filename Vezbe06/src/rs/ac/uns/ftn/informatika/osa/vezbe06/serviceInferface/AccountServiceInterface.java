package rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface;

import java.util.List;
import java.util.Optional;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;

public interface AccountServiceInterface {

	Account save(Account account);
    
	Account findByUsername(String username);

	Optional<Account> findOne(Integer accountId);
	
	List<Account> findAll();
	
	Account findByUsernameAndPassword(String username, String password);
	
}
