package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import java.io.Serializable;




@Entity
@Table(name = "tags")
public class Tag implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tag_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "tag_name", unique = false, nullable = false, length=100)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;
	
	@ManyToMany(mappedBy = "tags")
	private Set<Email> messages = new HashSet<Email>();
	
	public Tag() {

	}

	public Tag(Integer id, String name, User user, Set<Email> messages) {
		this.id = id;
		this.name = name;
		this.user = user;
		this.messages = messages;

	}

	public Integer getId() {
		return id;

	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;

	}

	public void setName(String name) {
		this.name = name;

	}
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	
	public Set<Email> getTagEmail() {
		return messages;
	}

	public void setTagEmail(Set<Email> messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "Tag{" +
				"id=" + id +
				", name='" + name + '\'' +
				", user='" + user + '\'' +
				", tagEmail='" + messages +
				'}';

	}

}