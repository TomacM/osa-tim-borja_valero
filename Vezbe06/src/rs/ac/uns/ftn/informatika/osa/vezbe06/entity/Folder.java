package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import javax.persistence.*;

import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.AccountDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.FolderDTO;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "folders")
public class Folder implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "folder_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "folder_name", unique = false, nullable = false, length=100)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
	private Account account;
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="folder" )
	private Set<Rule> rules = new HashSet<Rule>();
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="folder" )
	private Set<Email> messages = new HashSet<Email>();
	
	@ManyToOne
	@JoinColumn(name = "folder_parent_folder", referencedColumnName="folder_id", nullable=true)
	private Folder parent;
	
	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "parent")
	private Set<Folder> childrenFolder =new HashSet<Folder>();

	public Folder() {
	}

	public Folder(Integer id, String name, Account account,Set<Rule> rules,
			Set<Email> messages, Folder parent, Set<Folder> childrenFolder) {
		this.id = id;
		this.name = name;
		this.parent = parent;
		this.account = account;
		this.childrenFolder = childrenFolder;
	}
	
	public void add(Rule rule) {
		if (rule.getFolder() != null) {
			rule.getFolder().getFolderRule().remove(rule);
		}
		rule.setFolder(this);
		getFolderRule().add(rule);
	}
	
	public void add(Email message) {
		if (message.getFolder() != null) {
			message.getFolder().getMessages().remove(message);
		}
		message.setFolder(this);
		getMessages().add(message);
	}
	
	public void add(Folder folder) {
		if (folder.getParentFolder() != null) {
			folder.getParentFolder().getChildrenFolder().remove(folder);
		}
		folder.setParentFolder(this);
		getChildrenFolder().add(folder);
	}
	
	public void remove(Rule rule) {
		rule.setFolder(null);
		getFolderRule().remove(rule);
	}
	
	public void remove(Email message) {
		message.setFolder(null);
		getMessages().remove(message);
	}
	
	public void remove(Folder folder) {
		folder.setParentFolder(null);
		getChildrenFolder().remove(folder);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name;

	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Set<Rule> getFolderRule() {
		return rules;
	}

	public void setFolderRule(Set<Rule> rules) {
		this.rules = rules;
	}
	
	public Set<Email> getMessages() {
		return messages;
	}

	public void setMessages(Set<Email> messages) {
		this.messages = messages;
	}
	
	public Folder getParentFolder() {
		return parent;
	}

	public void setParentFolder(Folder parent) {
		this.parent = parent;
	}
	
	public Set<Folder> getChildrenFolder() {
		return childrenFolder;
	}

	public void setChildrenFolder(Set<Folder> childrenFolder) {
		this.childrenFolder = childrenFolder;
	}

	@Override
	public String toString() {
		return "Folder [id=" + id + ", name=" + name + ", account=" + account + ", rules=" + rules + ", messages="
				+ messages + ", parentFolder=" + parent + ", childrenFolder=" + childrenFolder + "]";
	}

}