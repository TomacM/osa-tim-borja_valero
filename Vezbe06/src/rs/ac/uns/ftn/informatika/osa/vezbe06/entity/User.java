package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToMany;

@Entity
@Table(name = "users")
public class User implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    private Integer id;
	
	@Column(name = "user_username", unique = false, nullable = false, length=40)
    private String username;

    @Column(name = "user_password", unique = false, nullable = false, length=20)
    private String password;

    @Column(name = "user_first_name", unique = false, nullable = false, length=100)
    private String firstname;

    @Column(name = "user_last_name", unique = false, nullable = false, length=100)
    private String lastname;
    
    @OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "user")
	private Set<Account> userAccount=new HashSet<Account>();
    
    @OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "user")
	private Set<Contact> userContact=new HashSet<Contact>();
    
    @OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "user")
	private Set<Tag> userTag = new HashSet<Tag>();
    
    public User() {}
    
    public User(Integer id, String username, String password, String firstname, String lastname, Set<Account> userAccount, Set<Contact> userContact, Set<Tag> userTag) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.userAccount = userAccount;
		this.userContact = userContact;
		this.userTag = userTag;
	}
    
	public void add(Contact contact) {
		if (contact.getUser() != null) {
			contact.getUser().getUserContact().remove(contact);
		}
		contact.setUser(this);
		getUserContact().add(contact);
	}
	
	public void add(Tag userTag) {
		if (userTag.getUser() != null) {
			userTag.getUser().getUserTag().remove(userTag);
		}
		userTag.setUser(this);
		getUserTag().add(userTag);
	}
	
	public void add(Account account) {
		if (account.getUser() != null) {
			account.getUser().getUserAccount().remove(account);
		}
		account.setUser(this);
		getUserAccount().add(account);
	}
	
	public void remove(Contact contact) {
		contact.setUser(null);
		getUserContact().remove(contact);
	}
	
	public void remove(Tag userTag) {
		userTag.setUser(null);
		getUserTag().remove(userTag);
	}
	
	public void remove(Account account) {
		account.setUser(null);
		getUserAccount().remove(account);
	}

    public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	
	 public Set<Account> getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(Set<Account> userAccount) {
		this.userAccount = userAccount;
	}

	
	
	public Set<Contact> getUserContact() {
		return userContact;
	}

	public void setUserContact(Set<Contact> userContact) {
		this.userContact = userContact;
	}

	
	
	public Set<Tag> getUserTag() {
		return userTag;
	}

	public void setUserTag(Set<Tag> userTag) {
		this.userTag = userTag;
	}

	@Override
	    public String toString() {
	        return "Account{" +
	                "id=" + id +
	                ", username='" + username + '\'' +
	                ", password=" + password +'\'' +
	                ", firstname=" + firstname+'\'' +
	                ", lastname='" + lastname + '\'' +
	                ", userAccount=" + userAccount + '\''+
	                ", userContact=" + userContact + '\'' +
	                ", userTag=" + userTag + 
	                '}';
	    }

}
