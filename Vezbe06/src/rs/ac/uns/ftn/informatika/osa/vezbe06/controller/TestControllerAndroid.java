//package rs.ac.uns.ftn.informatika.osa.vezbe06.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.AccountDTO;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.AttachmentDTO;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.Condition;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.ContactDTO;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.EmailDTO;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.FolderDTO;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.Operation;
//import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.RuleDTO;
//
//@RestController
//@RequestMapping(value="api")
//public class TestControllerAndroid {
//
//    AttachmentDTO attachment1 = new AttachmentDTO(1,"Tip 1","Attachment 1",null,null);
//    AttachmentDTO attachment2 = new AttachmentDTO(2,"Tip 2","Attachment 2",null,null);
//    AttachmentDTO attachment3 = new AttachmentDTO(3,"Tip 3","Attachment 3",null,null);
//    
//    
//
//	
//	AccountDTO account1 = new AccountDTO(1, null, "zure", "zure123", "zure@gmail.com","https://www.fancyhands.com/images/default-avatar-250x250.png?fbclid=IwAR11Wb8NtQgocEhZReAo-QL0UqRAOQLZdv8GGckqosxN7kbO-_zgbs35PDY");
//	AccountDTO account2 = new AccountDTO(2, null, "rade", "rade123", "rade@gmail.com","https://ya-webdesign.com/images250_/blank-profile-picture-png-8.png?fbclid=IwAR0a71fd37GCXZBrxMlxo_icCxQuKCQxNx2vthlL--D7YupOo_AUGuj6ack");
//	
//
//    ContactDTO contact1 = new ContactDTO(1,"Zure","Zure","zure@gmail.com",null,null,account2);
//    ContactDTO contact2 = new ContactDTO(2,"Radule","Rade","rade@gmail.com",null,null,account1);
//	
//	FolderDTO folder1 = new FolderDTO(1, "Folder 1", null, null,account1);
//	FolderDTO folder2 = new FolderDTO(2, "Folder 2", null, folder1,account1);
//	
//	FolderDTO folder3 = new FolderDTO(3, "Folder 3", null, null,account2);
//	FolderDTO folder4 = new FolderDTO(4, "Folder 4", null, folder3,account2);
//	RuleDTO rule5= new RuleDTO(1, Condition.CC, Operation.COPY, null);
//	
//	EmailDTO email1 = new EmailDTO(1,contact1,contact2,null,null,"14/04/2019 12:00:00","Email 1","Cao Rade",null,null,folder1,true);
//	EmailDTO email2 = new EmailDTO(2,contact2,contact1,null,null,"16/04/2019 15:00:00","Email 2","Cao Zure",null,null,null,false);
//	EmailDTO email3 = new EmailDTO(3,contact2,contact1,null,null,"15/04/2019 20:00:00","Email 3","Gubljenje max",null,null,folder1,true);
//	EmailDTO email4 = new EmailDTO(4,contact1,contact2,null,null,"31/04/2019 21:25:00","Email 4","Secer 3000 obrtaja",null,null,folder1,false);
//	EmailDTO email5 = new EmailDTO(5,contact1,contact2,null,null,"21/04/2019 21:00:00","Email 5","Borja Valero",null,null,folder1,true);
//	EmailDTO email6 = new EmailDTO(6,contact2,contact1,null,null,"21/04/2019 23:00:00","Email 6","Tomac glavonja",null,null,null,false);
//	
//	
//	List<ContactDTO> contacts = new ArrayList<>();
//	List<EmailDTO> emailsDTO = new ArrayList<EmailDTO>();
//	List<FolderDTO> folderDTO = new ArrayList<>();
//	List<RuleDTO> rules = new ArrayList<>();
//	List<AccountDTO> accountDTO=new ArrayList<>();
//
//	@GetMapping("/emails")
//	public ResponseEntity<List<EmailDTO>> getEmails() {
//
////	    List<Attachment> atts2 = new ArrayList<>();
////        atts.add(attachment1);
////        atts.add(attachment2);
////        atts2.add(attachment3);       
//        
//		if(emailsDTO.isEmpty()) {
//	        emailsDTO.add(email1);
//			emailsDTO.add(email2);
//			emailsDTO.add(email3);
//			emailsDTO.add(email4);
//			emailsDTO.add(email5);
//			emailsDTO.add(email6);
//		}
//
//		return new ResponseEntity<List<EmailDTO>>(emailsDTO, HttpStatus.OK);
//	}
//	
//	@GetMapping("/accounts")
//	public ResponseEntity<List<AccountDTO>> getAccounts() {
//		if(accountDTO.isEmpty()) {
//			accountDTO.add(account1);
//			accountDTO.add(account2);
//		}
//		return new ResponseEntity<List<AccountDTO>>(accountDTO, HttpStatus.OK);
//	}
//	
//	@GetMapping("/folders")
//	public ResponseEntity<List<FolderDTO>> getFolders() {
//		if (folderDTO.isEmpty()) {
//			folderDTO.add(folder1);
//			folderDTO.add(folder2);
//			folderDTO.add(folder3);
//			folderDTO.add(folder4);
//		}
//		return new ResponseEntity<List<FolderDTO>>(folderDTO, HttpStatus.OK);
//	}
//	
//	@GetMapping("/rules")
//	public ResponseEntity<List<RuleDTO>> getRules() {
//		if (rules.isEmpty()) {
//			rules.add(rule5);
//		}
//		return new ResponseEntity<List<RuleDTO>>(rules, HttpStatus.OK);
//	}
//	
//	
//	@GetMapping("/contacts")
//	public ResponseEntity<List<ContactDTO>> getContacts() {
//
//		if (contacts.isEmpty()) {
//			contacts.add(contact1);
//			contacts.add(contact2);
//		}
//		return new ResponseEntity<List<ContactDTO>>(contacts, HttpStatus.OK);
//	}
//	
//    @PostMapping(value="/addcontacts", consumes="application/json")
//    public ResponseEntity<ContactDTO> addContact(@RequestBody ContactDTO contactDTO){
//
//        ContactDTO contact = new ContactDTO();
//
//        contact.setId(contactDTO.getId());
//        contact.setFirstName(contactDTO.getFirstName());
//        contact.setLastName(contactDTO.getLastName());
//        contact.setEmail(contactDTO.getEmail());
//        contact.setDisplay(null);
//        contact.setFormat(null);
//        contact.setAcc(contactDTO.getAcc());
//        
//        contacts.add(contact);
//        
//        return new ResponseEntity<ContactDTO>(contact, HttpStatus.CREATED);
//        
//} 
//    @PostMapping(value="/addemails", consumes="application/json")
//    public ResponseEntity<EmailDTO> addEmail(@RequestBody EmailDTO email){
//
//        EmailDTO email1 = new EmailDTO();
//
//        email1.setId(email.getId());
//        email1.setFrom(email.getFrom());
//        email1.setTo(email.getTo());
//        email1.setCc(null);
//        email1.setBcc(null);
//        email1.setDateTime(email.getDateTime());
//        email1.setSubject(email.getSubject());
//        email1.setContent(email.getContent());
//        email1.setTagovi(null);
//        email1.setIdAttachmentId(null);
//        email1.setFolder(null);
//        email1.setProcitana(false);
//        
//        emailsDTO.add(email1);
//        
//        return new ResponseEntity<EmailDTO>(email1, HttpStatus.CREATED);
//        
//} 
//    
//    @PutMapping(value="/updatecontacts/{id}", consumes="application/json")
//    public ResponseEntity<ContactDTO> updateContact(@RequestBody ContactDTO contact,@PathVariable("id") int id){
//		for (ContactDTO c : contacts) {
//			if (c.getId() == id) {
//		        c.setFirstName(contact.getFirstName());
//		        c.setLastName(contact.getLastName());
//		        c.setEmail(contact.getEmail());
//		        c.setDisplay(null);
//		        c.setFormat(null);
//			}
//		}
//        return new ResponseEntity<ContactDTO>(HttpStatus.OK);
//}
//    
//    @PutMapping(value="/updateaccount/{id}", consumes="application/json")
//    public ResponseEntity<AccountDTO> updateAccount(@RequestBody AccountDTO account,@PathVariable("id") int id){
//		for (AccountDTO a : accountDTO) {
//			if (a.getId() == id) {
//				a.setSmtp(null);
//		        a.setUsername(account.getUsername());
//		        a.setPassword(account.getPassword());
//		        a.setEmail(account.getEmail());
//		        a.setPhoto(account.getPhoto());
//			}
//		}
//        return new ResponseEntity<AccountDTO>(HttpStatus.OK);
//}
//    
//    @PutMapping(value="/updatefolder/{id}", consumes="application/json")
//    public ResponseEntity<FolderDTO> updateFolder(@RequestBody FolderDTO folder,@PathVariable("id") int id){
//		for (FolderDTO f : folderDTO) {
//			if (f.getId() == id) {
//				f.setDestination(folder.getDestination());
//				f.setName(folder.getName());
//			}
//		}
//        return new ResponseEntity<FolderDTO>(HttpStatus.OK);
//}
//    
//    @PutMapping(value="/updateemails/{id}", consumes="application/json")
//    public ResponseEntity<EmailDTO> updateEmail(@RequestBody EmailDTO email,@PathVariable("id") int id){
//		for (EmailDTO e : emailsDTO) {
//			if (e.getId() == id) {
//				e.setBcc(email.getBcc());
//				e.setCc(email.getCc());
//				e.setContent(email.getContent());
//				e.setDateTime(email.getDateTime());
//				e.setFolder(email.getFolder());
//				e.setFrom(email.getFrom());
//				e.setId(email.getId());
//				e.setIdAttachmentId(email.getIdAttachmentId());
//				e.setProcitana(email.isProcitana());
//				e.setSubject(email.getSubject());
//				e.setTagovi(email.getTagovi());
//				e.setTo(email.getTo());
//		
//			}
//		}
//        return new ResponseEntity<EmailDTO>(HttpStatus.OK);
//}
//    
//    @PostMapping(value="/addfolder", consumes="application/json")
//public ResponseEntity<FolderDTO> addFolder(@RequestBody FolderDTO folderDTO1){
//
//    FolderDTO folder = new FolderDTO();
//    
//    folder.setId(folderDTO1.getId());
//    folder.setName(folderDTO1.getName());
//    folder.setDestination(folderDTO1.getDestination());
//    folder.setNadfolder(folderDTO1.getNadfolder());
//    folder.setAcc(folderDTO1.getAcc());
//
//    folderDTO.add(folder);
//    
//    return new ResponseEntity<FolderDTO>(folder, HttpStatus.CREATED);
//    
//}
//    @PostMapping(value="/addrule", consumes="application/json")
//public ResponseEntity<RuleDTO> addRule(@RequestBody RuleDTO ruleDto){
//
//    RuleDTO rule = new RuleDTO();
//    
//    rule.setId(ruleDto.getId());
//    rule.setCondition(ruleDto.getCondition());
//    rule.setOperation(rule.getOperation());
//    rule.setDestionation(rule.getDestionation());
//    rules.add(rule);
//    
//    return new ResponseEntity<RuleDTO>(rule, HttpStatus.CREATED);
//    
//}
//	
//	
//	
//	//             -----------------------------------------------------------
//	//             ------------------------ BRISANJE -------------------------
//	//             -----------------------------------------------------------
//	
//    @DeleteMapping("/deletecontacts/{id}")
//    public ResponseEntity<ContactDTO> deleteContact(@PathVariable int id){
//		for (ContactDTO c : contacts) {
//			if (c.getId() == id) {
//				contacts.remove(c);
//			}
//		}
//		return new ResponseEntity<ContactDTO>(HttpStatus.OK);
//}
//    
//    @DeleteMapping("/deleterule/{id}")
//    public ResponseEntity<RuleDTO> deleteRule(@PathVariable int id){
//		for (RuleDTO r : rules) {
//			if (r.getId() == id) {
//				rules.remove(r);
//			}
//		}
//		return new ResponseEntity<RuleDTO>(HttpStatus.OK);
//}
//    
//    @DeleteMapping("/deleteemails/{id}")
//    public ResponseEntity<EmailDTO> deleteEmail(@PathVariable int id){
//		for (EmailDTO e : emailsDTO) {
//			if (e.getId() == id) {
//				emailsDTO.remove(e);
//			}
//		}
//		return new ResponseEntity<EmailDTO>(HttpStatus.OK);
//}
//    
//    @DeleteMapping("/deletefolders/{id}")
//    public ResponseEntity<FolderDTO> deleteFolder(@PathVariable int id){
//		for (FolderDTO f : folderDTO) {
//			if (f.getId() == id) {
//				folderDTO.remove(f);
//			}
//		}
//		return new ResponseEntity<FolderDTO>(HttpStatus.OK);
//}
//	
//}
