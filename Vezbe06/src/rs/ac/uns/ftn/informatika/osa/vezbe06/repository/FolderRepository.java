package rs.ac.uns.ftn.informatika.osa.vezbe06.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder;

import java.util.List;

public interface FolderRepository extends JpaRepository<Folder, Integer> {
    
	
	List<Folder> findByParent(Folder parent);
	
	Folder findByName(String name);
	
	Folder findByNameAndAccount(String name, Account account);
	
}
