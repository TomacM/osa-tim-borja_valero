package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Attachment;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Tag;

public class EmailDTO implements Serializable {

    private Integer id;
    private String from;
    private String to;
    private List<String> cc;
    private List<String> bcc;
    private Date dateTime;
    private String subject;
    private String content;
    private List<AttachmentDTO> attachments;
    private List<TagDTO> tags;
    private boolean procitano;
    
    
	public EmailDTO() {
		super();
	}
	
    public EmailDTO(Integer id, String from, String to, List<String> cc, List<String> bcc, Date dateTime,
			String subject, String content, List<AttachmentDTO> attachments, List<TagDTO> tags, boolean procitano) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.content = content;
		this.attachments = attachments;
		this.tags = tags;
		this.procitano = procitano;
	}

    public EmailDTO(Email message) {
    	List<AttachmentDTO> attachmentDTOs = new ArrayList<>();
    	for (Attachment itAttachment : message.getMessageAttachment()) {
    		attachmentDTOs.add(new AttachmentDTO(itAttachment));
		}
    	List<TagDTO> tagDTOs = new ArrayList<>();
    	if (message.getTags() != null) {
        	for (Tag itTag : message.getTags()) {
    			tagDTOs.add(new TagDTO(itTag));
    		}
		}
    	if (message.getId() != null) {
    		this.setId(message.getId());
		}
    	
    	this.setFrom(message.getFrom());
    	this.setTo(message.getTo());

    	this.setCc(new ArrayList<>());
    	if (message.getCc() != null && !message.getCc().equals("")) {
			StringTokenizer token =  new StringTokenizer(message.getCc(), ";");
			while (token.hasMoreTokens()) {
				this.getCc().add(token.nextToken());
			}
		}
    	this.setBcc(new ArrayList<>());
    	if (message.getBcc() != null && !message.getBcc().equals("")) {
			StringTokenizer token =  new StringTokenizer(message.getBcc(), ";");
			while (token.hasMoreTokens()) {
				this.getBcc().add(token.nextToken());
			}
		}
    	

    	
/*        Date date = message.getDateTime();  
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSSZ");  
        String strDate = dateFormat.format(date);
        
    	DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSSZ", Locale.ENGLISH);
    	DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
    	LocalDateTime date1 = LocalDateTime.parse(strDate, inputFormatter);
    	String formattedDate = outputFormatter.format(date1);
    	
    	String target = formattedDate;
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
		try {
			Date result = df.parse(target);
			System.out.println(result);
			this.setDateTime(result);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  */
    	
    	this.setDateTime(message.getDateTime());

    	
    	this.setSubject(message.getSubject());
    	this.setContent(message.getContent());
    	this.setAttachments(attachmentDTOs);
    	this.setTags(tagDTOs);
    	this.setProcitano(message.getUnread());
    	
    }









	public Integer getId() {
		return id;
	}









	public void setId(Integer id) {
		this.id = id;
	}









	public String getFrom() {
		return from;
	}









	public void setFrom(String from) {
		this.from = from;
	}









	public String getTo() {
		return to;
	}









	public void setTo(String to) {
		this.to = to;
	}









	public List<String> getCc() {
		return cc;
	}









	public void setCc(List<String> cc) {
		this.cc = cc;
	}









	public List<String> getBcc() {
		return bcc;
	}









	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}









	public Date getDateTime() {
		return dateTime;
	}









	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}









	public String getSubject() {
		return subject;
	}









	public void setSubject(String subject) {
		this.subject = subject;
	}









	public String getContent() {
		return content;
	}









	public void setContent(String content) {
		this.content = content;
	}









	public List<AttachmentDTO> getAttachments() {
		return attachments;
	}









	public void setAttachments(List<AttachmentDTO> attachments) {
		this.attachments = attachments;
	}









	public List<TagDTO> getTags() {
		return tags;
	}









	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}









	public boolean isProcitano() {
		return procitano;
	}









	public void setProcitano(boolean procitano) {
		this.procitano = procitano;
	}






	
	
}
