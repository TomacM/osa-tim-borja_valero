package rs.ac.uns.ftn.informatika.osa.vezbe06.mailApi;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.MessageServiceInterface;

public class MailSender {
	private final MessageServiceInterface messageService;
	
	public MailSender(MessageServiceInterface messageService) {
		this.messageService = messageService;
	}
	
	public void sendEmail(Email message) {
		
		//Declare to
		String EMAIL_TO = message.getTo();
		String EMAIL_FROM = message.getFrom();
		
		final String username = message.getAccount().getUsername() + "@gmail.com";
		final String password = message.getAccount().getPassword();
		
		//Set properties and their values
		Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
        prop.put("mail.smtp.socketFactory.port", "465"); //SSL Port
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        prop.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.port", "465"); //SMTP Port
        prop.put("mail.smtp.ssl.enable", "true");
        
        //Create a Session object & authenticate uid and pwd
        Session session = Session.getInstance(prop, new Authenticator() {
        	protected PasswordAuthentication getPasswordAuthentication() {
        		return new PasswordAuthentication(username, password);
        	}
		});
        
        try {
			//Create MimeMessage object & set values
        	MimeMessage messageObj = new MimeMessage(session);
        	
        	messageObj.setFrom(new InternetAddress(EMAIL_FROM));
            messageObj.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(EMAIL_TO));
            messageObj.setSubject(message.getSubject());
            
            //Declare text values
            String messageContent = message.getContent() + "\n";
            
            Multipart mp = new MimeMultipart();
            BodyPart messageBody = new MimeBodyPart();
            
            messageBody.setText(messageContent);
            mp.addBodyPart(messageBody);
            messageObj.setContent(mp);
            Transport.send(messageObj);
            System.out.println(message.getFrom());
            message = messageService.save(message);
        	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
}

