package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import javax.persistence.*;

import java.io.Serializable;

@Entity

@Table(name = "attachments")

public class Attachment implements Serializable {

	@Id

	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "attachment_id", unique = true, nullable = false)

	private Integer id;

	@Column(name = "attachment_data", unique = false, nullable = true)

	private String data;

	@Column(name = "attachment_type", unique = false, nullable = false, length=20)

	private String type;

	@Column(name = "attachment_name", unique = false, nullable = false, length=100)

	private String name;

	@ManyToOne
	@JoinColumn(name = "message_id", referencedColumnName = "message_id", nullable = false)
	private Email email;

	public Attachment() {

	}

	public Attachment(Integer id, String data, String type, String name, Email email) {

		this.id = id;

		this.data = data;

		this.type = type;

		this.name = name;

		this.email = email;

	}

	public Integer getId() {

		return id;

	}

	public void setId(Integer id) {

		this.id = id;

	}

	public String getData() {

		return data;

	}

	public void setData(String data) {

		this.data = data;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	@Override

	public String toString() {

		return "Attachment{" +

				"id=" + id +

				", data='" + data + '\'' +

				", type='" + type + '\'' +

				", name='" + name + '\'' +

				", email=" + email +

				'}';

	}

}
