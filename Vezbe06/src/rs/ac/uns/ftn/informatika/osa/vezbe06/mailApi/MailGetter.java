package rs.ac.uns.ftn.informatika.osa.vezbe06.mailApi;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.EmailDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.AccountServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.FolderServiceInterface;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.MessageServiceInterface;

@Component
public class MailGetter {
	
	private MessageServiceInterface messageService;
	private FolderServiceInterface folderService;
	private AccountServiceInterface accountService;
	
	public MailGetter(MessageServiceInterface messageService, FolderServiceInterface folderService) {
		this.messageService = messageService;
		this.folderService = folderService;
	}

	public void loadEmails(Account account) {
		try {
			
			Properties prop = new Properties();
	        prop.put("mail.imap.host", "imap.gmail.com");
	        prop.put("mail.imap.port", "993");
	        prop.put("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        prop.setProperty("mail.imap.socketFactory.fallback", "false");
	        prop.setProperty("mail.imap.socketFactory.port", "993");
	        
	        Session session = Session.getDefaultInstance(prop, null);
	        Store store = session.getStore("imaps");
	        store.connect("imap.googlemail.com", 993, account.getUsername() + "@gmail.com", account.getPassword());
	        Folder emailFolder = store.getFolder("inbox");
	        emailFolder.open(Folder.READ_ONLY);
	        javax.mail.Message[] messages = emailFolder.getMessages();

			List<EmailDTO> messageDTOs = new ArrayList<>();
			for (Email itMessage : account.getAccountEmail()) {
				if (itMessage.getFolder().getName().equals("Inbox")) {
					messageDTOs.add(new EmailDTO(itMessage));
				}

			}
			
	        System.out.println(messageDTOs.size());
	        System.out.println(messages.length);
	        
	        for (javax.mail.Message message : messages) {
	        	if(messageDTOs.size() < messages.length) {
					Email temp = new Email();
					temp.setFrom(message.getFrom()[0].toString());
					temp.setTo(message.getReplyTo()[0].toString());
					temp.setCc("");
					temp.setBcc("");
					temp.setDateTime(message.getSentDate());
					temp.setSubject(message.getSubject());
					
					String result = "";
					try {
						MimeMultipart body = (MimeMultipart) message.getContent();
						for (int i = 0; i < body.getCount(); i++) {
							MimeBodyPart bodyPart= (MimeBodyPart) body.getBodyPart(i);
                            if (bodyPart.isMimeType("text/plain")) {
                                result += "\n" + bodyPart.getContent();
                                break; // without break same text appears twice in my tests
                            } else if (bodyPart.isMimeType("text/html")) {
                                result = (String) bodyPart.getContent();
                            }
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				
					temp.setContent(result);
					rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder inboxFolder = folderService.findByNameAndAccount("Inbox", account);
					inboxFolder.add(temp);
					temp.setUnread(false);
					account.add(temp);
					messageService.save(temp);
	        	}
			}
	        emailFolder.close();
	        store.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
