package rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface;

import java.util.List;
import java.util.Optional;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Contact;

public interface ContactServiceInterface {
	
	Optional<Contact> findOne(Integer contactId);
	
    List<Contact> findAll();
    
    Contact save(Contact contact);
    
    void remove(Integer contactId);
	
    
}
