package rs.ac.uns.ftn.informatika.osa.vezbe06.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.repository.AccountRepository;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.AccountServiceInterface;

@Service
public class AccountService implements AccountServiceInterface{

		@Autowired
		private AccountRepository accountRepository;

		@Override
		public Account save(Account account) {
			return accountRepository.save(account);
			
		}

		@Override
		public Account findByUsername(String username) {
			return accountRepository.findByUsername(username);
		}

		@Override
		public Optional<Account> findOne(Integer accountId) {
			
			return accountRepository.findById(accountId);
		}

		@Override
		public List<Account> findAll() {
			return accountRepository.findAll();
		}

		@Override
		public Account findByUsernameAndPassword(String username, String password) {
			return accountRepository.findByUsernameAndPassword(username, password);
		}

	}

