package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Email;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.User;

public class AccountDTO implements Serializable {
	private Integer id;
    private String smtpAddress;
    private String pop3Imap;
    private String username;
    private String password;
    private String displayName;
    private List<EmailDTO> messages;


    
    public AccountDTO() {
    }

    public AccountDTO(Integer id, String smtpAddress, String pop3Imap, String username,String token, String password,String displayName, List<EmailDTO> messages) {
        this.id = id;
        this.smtpAddress = smtpAddress;
        this.pop3Imap = pop3Imap;
        this.username = username;
        this.password = password;
        this.displayName = displayName;
        this.messages = messages;
    }
    
    public AccountDTO(Account account) {
    	User user = account.getUser();
    	List<EmailDTO> emailDTOs = new ArrayList<>(); 
    	if (account.getAccountEmail() != null) {
	    	for (Email itMessage : account.getAccountEmail()) {
	    		emailDTOs.add(new EmailDTO(itMessage));
			}	
		}
    	this.setId(user.getId());
    	this.setSmtpAddress(account.getSmtpAddress());
    	this.setPop3Imap(account.getPop3Imap());
    	this.setUsername(account.getUsername());
    	this.setPassword(account.getPassword());
    	this.setMessages(emailDTOs);
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSmtpAddress() {
		return smtpAddress;
	}

	public void setSmtpAddress(String smtpAddress) {
		this.smtpAddress = smtpAddress;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
    public List<EmailDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<EmailDTO> messages) {
        this.messages = messages;
    }

	public String getPop3Imap() {
		return pop3Imap;
	}

	public void setPop3Imap(String pop3Imap) {
		this.pop3Imap = pop3Imap;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
    
	

	}
