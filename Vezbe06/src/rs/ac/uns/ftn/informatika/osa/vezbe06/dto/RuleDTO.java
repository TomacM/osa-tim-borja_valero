package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule.Condition;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Rule.Operation;

public class RuleDTO {
    private Integer id;
    private Condition condition;
    private Operation operation;

	public RuleDTO() {
	}



	public RuleDTO(Integer id, Condition condition, Operation operation) {
		this.id = id;
		this.condition = condition;
		this.operation = operation;
	}
    
    public RuleDTO(Rule rule) {
    	
    	this(rule.getId(), rule.getCondition(), rule.getOperation());
    }


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	

}
