package rs.ac.uns.ftn.informatika.osa.vezbe06.dto;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Contact;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Contact.Format;

public class ContactDTO {
	
    private Integer id;
    private String firstName;
    private String lastName;
    private String display;
    private String email;
    private Format format;
    private String note;
    private PhotoDTO photo;
    
	public ContactDTO() {
		super();
	}
	

	
    public ContactDTO(Integer id, String firstName, String lastName, String display, String email, Format format,String note, PhotoDTO photo) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.display = display;
		this.email = email;
		this.format = format;
		this.note = note;
		this.photo = photo;
	}



	public ContactDTO(Contact contact) {
    	this.setId(contact.getId());
    	this.setFirstName(contact.getFirstName());
    	this.setLastName(contact.getLastName());
    	this.setDisplay(contact.getDisplay());
    	this.setEmail(contact.getEmail());
    	this.setFormat(contact.getFormat());
    	this.setNote(contact.getNote());
    	
    	if (contact.getContactPhoto() != null && contact.getContactPhoto().iterator().hasNext()) {
    		this.setPhoto(new PhotoDTO(contact.getContactPhoto().iterator().next()));
		} else {
			this.setPhoto(new PhotoDTO());
		}
    	
    	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}



	public Format getFormat() {
		return format;
	}



	public void setFormat(Format format) {
		this.format = format;
	}


	public String getNote() {
		return note;
	}



	public void setNote(String note) {
		this.note = note;
	}



	public PhotoDTO getPhoto() {
		return photo;
	}



	public void setPhoto(PhotoDTO photo) {
		this.photo = photo;
	}


	
	

	

}
