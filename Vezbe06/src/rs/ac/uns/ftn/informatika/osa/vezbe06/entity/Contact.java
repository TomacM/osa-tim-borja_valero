package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import javax.persistence.*;

import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.UserDTO;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static javax.persistence.CascadeType.ALL;

@Entity
@Table(name = "contacts")
public class Contact implements Serializable {

	public enum Format {
		PLAIN,
		HTML
	}
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "contact_first_name", unique = false, nullable = false, length=100)
    private String firstName;

    @Column(name = "contact_last_name", unique = false, nullable = false, length=100)
    private String lastName;

    @Column(name = "contact_email", unique = true, nullable = false, length=100)
    private String email;

    @Column(name = "contact_display", unique = false, nullable = false, length=100)
    private String display;

    @Column(name = "contact_note", unique = false, nullable = true, length=100)
    @Lob
    private String note;

    @Column(name="contact_format", unique=false, nullable=true)
	private Format format;
    
    @OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy = "contact")
    private Set<Photo> contactPhoto = new HashSet<Photo>();
    
    @ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;


//
//    public Contact(Long id, String firstName, String lastName, String email, String display, String note) {
//        this.id = id;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.display = display;
//    }
    public Contact(Integer id, String firstName, String lastName, String email, String display, String note, Format format, Set<Photo> contactPhoto, User user) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.display = display;
        this.note = note;
        this.format = format;
        this.contactPhoto = contactPhoto;
        this.user = user;
    }
    public Contact() {
    }
    
	public void add(Photo photo) {
		if (photo.getContact() != null) {
			photo.getContact().getContactPhoto().remove(photo);
		}
		photo.setContact(this);
		getContactPhoto().add(photo);
	}
	
	public void remove(Photo photo) {
		photo.setContact(null);
		getContactPhoto().remove(photo);
	}
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getDisplay() {
        return display;
    }
    public void setDisplay(String display) {
        this.display = display;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    
	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}
    
    public Set<Photo> getContactPhoto() {
        return contactPhoto;
    }
    public void setContactPhoto(Set<Photo> contactPhoto) {
        this.contactPhoto = contactPhoto;
    }
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", display='" + display + '\'' +
                ", note='" + note + '\'' +
                ", format='" + format + '\'' +
                ", contactPhoto=" + contactPhoto + '\'' +
                ", user=" + user +
                '}';

    }
}
