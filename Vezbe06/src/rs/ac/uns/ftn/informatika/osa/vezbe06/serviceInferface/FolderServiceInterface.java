package rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface;

import java.util.List;
import java.util.Optional;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Account;
import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Folder;

public interface FolderServiceInterface {
	
	List<Folder> findByParent(Folder parent);
	
	Optional<Folder> findOne(Integer folderId);
	
	List<Folder> findAll();
	
	Folder save(Folder folder);
	
	void remove(Integer id);
	
	Folder findByName(String name);
	
	Folder findByNameAndAccount(String name, Account account);
}
