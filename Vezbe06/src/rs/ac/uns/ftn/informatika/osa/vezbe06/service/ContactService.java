package rs.ac.uns.ftn.informatika.osa.vezbe06.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Contact;
import rs.ac.uns.ftn.informatika.osa.vezbe06.repository.ContactRepository;
import rs.ac.uns.ftn.informatika.osa.vezbe06.serviceInferface.ContactServiceInterface;

@Service
public class ContactService implements ContactServiceInterface {

	@Autowired
	ContactRepository contactRepository;

	@Override
	public Optional<Contact> findOne(Integer contactId) {
		
		return contactRepository.findById(contactId);
	}

	@Override
	public List<Contact> findAll() {
		return contactRepository.findAll();
	}

	@Override
	public Contact save(Contact contact) {
		return contactRepository.save(contact);
	}

	@Override
	public void remove(Integer id) {
		contactRepository.deleteById(id);
	}
	}
