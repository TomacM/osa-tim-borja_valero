package rs.ac.uns.ftn.informatika.osa.vezbe06.entity;

import javax.persistence.*;

import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.AccountDTO;
import rs.ac.uns.ftn.informatika.osa.vezbe06.dto.FolderDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Entity

@Table(name = "messages")

public class Email implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "message_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "message_from", unique = false, nullable = false, length=100)
	private String from;

	@Column(name = "message_to", unique = false, nullable = false)
	private String to;

	@Column(name = "message_cc", unique = false, nullable = true)
	private String cc;

	@Column(name = "message_bcc", unique = false, nullable = true)
	private String bcc;

	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "message_date_time", unique = false, nullable = false)
	private Date dateTime;

	@Column(name = "message_subject", unique = false, nullable = false, length=250)
	private String subject;

	@Column(name = "message_content", unique = false, nullable = false)
	@Lob
	private String content;

	@Column(name = "message_unread", unique = false, nullable = false)
	private Boolean unread;

	@OneToMany(cascade = { ALL }, fetch = FetchType.LAZY, mappedBy = "email")
	private Set<Attachment> messageAttachment = new HashSet<Attachment>();
	
	@ManyToOne
	@JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
	private Account account;
	
	@ManyToOne
	@JoinColumn(name = "folder_id", referencedColumnName = "folder_id", nullable = false)
	private Folder folder;
	
	@ManyToMany
	@JoinTable(name = "message_tags", joinColumns = @JoinColumn(name="message_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private Set<Tag> tags = new HashSet<Tag>();

	public Email() {

	}

	public Email(Integer id, String from, String to, String cc, String bcc, Date dateTime, String subject,
			String content, Boolean unread, Set<Tag> tags, Set<Attachment> messageAttachment, Account account, Folder folder) {

		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.content = content;
		this.unread = unread;
		this.tags = tags;
		this.messageAttachment = messageAttachment;
		this.account = account;
		this.folder = folder;
	}
	
	public void add(Attachment attachment) {
		if (attachment.getEmail() != null) {
			attachment.getEmail().getMessageAttachment().remove(attachment);
		}
		attachment.setEmail(this);
		getMessageAttachment().add(attachment);
	}
	
	public void remove(Attachment attachment) {
		attachment.setEmail(null);
		getMessageAttachment().remove(attachment);
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Boolean getUnread() {
		return unread;
	}
	

	public void setUnread(Boolean unread) {
		this.unread = unread;
	}

	public Set<Attachment> getMessageAttachment() {
		return messageAttachment;
	}
	public void setMessageAttachment(Set<Attachment> messageAttachment) {
		this.messageAttachment = messageAttachment;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag>tags) {
		
		this.tags = tags;
	}
	
	@Override
	public String toString() {
		return "Message{" +
	
				"id=" + id +
				", from='" + from + '\'' +
				", to='" + to + '\'' +
				", cc='" + cc + '\'' +
				", bcc='" + bcc + '\'' +
				", dateTime=" + dateTime +
				", subject='" + subject + '\'' +
				", content='" + content + '\'' +
				", unread=" + unread + '\'' +
				", tags=" + tags + '\'' +
				", messageAttachment=" + messageAttachment + '\'' +
				", account=" + account + '\'' +
				", folder=" + folder +		
				'}';
	}

}
