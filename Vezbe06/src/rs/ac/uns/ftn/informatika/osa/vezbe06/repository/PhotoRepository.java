package rs.ac.uns.ftn.informatika.osa.vezbe06.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Integer> {

	
	@Override
	public Photo save(Photo photo);
}