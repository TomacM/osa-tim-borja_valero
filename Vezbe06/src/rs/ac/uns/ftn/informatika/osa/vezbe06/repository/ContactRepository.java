package rs.ac.uns.ftn.informatika.osa.vezbe06.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.ac.uns.ftn.informatika.osa.vezbe06.entity.Contact;


public interface ContactRepository extends JpaRepository<Contact, Integer> {

	Contact findByEmail(String email);
	
	
}
