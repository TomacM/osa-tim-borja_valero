DROP SCHEMA IF EXISTS osa;
CREATE SCHEMA osa DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE osa;
create table accounts(
 account_id bigint auto_increment,
 account_smtp_address varchar(250),
 account_smtp_port int,
 account_in_server_type int,
  account_pop3_imap varchar(10),
 account_in_server_address varchar(250),
 account_in_server_port int,
 account_username varchar(40),
 account_password varchar(20),
 account_display_name varchar(100),
 user_id bigint,
 primary key (account_id)
);
insert into accounts(account_smtp_address, account_smtp_port, account_in_server_type, account_pop3_imap, account_in_server_address, account_in_server_port,account_username,account_password,account_display_name,user_id) values
("adminosatest619",2233,123,"pop3","dddosatest619",2230,"osatest619","jakalozinka123","Osa test",1);
insert into accounts(account_smtp_address, account_smtp_port, account_in_server_type, account_pop3_imap, account_in_server_address, account_in_server_port,account_username,account_password,account_display_name,user_id) values
("adminzure",2233,123,"pop3","dddzure",2230,"zure","zure","Dusan Zurkovic",2);
insert into accounts(account_smtp_address, account_smtp_port, account_in_server_type, account_pop3_imap, account_in_server_address, account_in_server_port,account_username,account_password,account_display_name,user_id) values
("sarhitekture",2233,123,"pop3","dddsarhitekture",2230,"sarhitekture","jakalozinka","S Arhitekture",3);
create table users(
	user_id bigint auto_increment,
	user_username varchar(40),
	user_password varchar(20),
	user_first_name varchar(20),
	user_last_name varchar(20),
	primary key (user_id)
);
insert into users(user_username,user_password, user_first_name, user_last_name) values
("osatest619","jakalozinka123","osatest619","osatest619");
insert into users(user_username,user_password, user_first_name, user_last_name) values
("zure","zure","zure","zure");
insert into users(user_username,user_password, user_first_name, user_last_name) values
("sarhitekture","jakalozinka","sarhitekture","sarhitekture");

create table attachments(
	attachment_id bigint auto_increment,
	attachment_data text,
	attachment_type varchar(20),
	attachment_name varchar(100),
	message_id bigint,
	primary key (attachment_id)
);
insert into attachments(attachment_data, attachment_type, attachment_name, message_id) values
(null,null,"Gledam drinu njene vode plave",1);
insert into attachments(attachment_data, attachment_type, attachment_name, message_id) values
(null,null,"Svi kockari gube sve",2);
insert into attachments(attachment_data, attachment_type, attachment_name, message_id) values
(null,null,"Placi mala placi",3);

create table contacts(
   contact_id bigint auto_increment,
   contact_first_name varchar(100),
   contact_last_name varchar(100),
   contact_email varchar(100), 
   contact_display varchar(100),
   contact_note varchar(100),
   contact_format varchar(30),
   user_id bigint,
   primary key (contact_id)
);
insert into contacts(contact_first_name, contact_last_name, contact_email, contact_display,contact_note,contact_format,user_id) values
("Raca","Racic","raca@gmail.com","Raca123","Najbolji dugar sa faksa dobar sam sa njim i on me jako gotivi",null,3);
insert into contacts(contact_first_name, contact_last_name, contact_email, contact_display,contact_note,contact_format,user_id) values
("Rade","Obradovic","rade@gmail.com","Dera123","no koment",null,1);
insert into contacts(contact_first_name, contact_last_name, contact_email, contact_display,contact_note,contact_format,user_id) values
("Zure","Zurkovic","zure@gmail.com","ZureJzma","Kakva si kao andjeo",null,2);

create table messages(
	message_id bigint auto_increment,
    message_from varchar(100),
    message_to  text,
    message_cc  text,
    message_bcc  text,
    message_date_time DateTime,
    message_subject varchar(250),
    message_content text,
    message_unread boolean,
    account_id bigint,
    folder_id bigint,
    primary key (message_id)
);

insert into messages(message_from, message_to, message_cc, message_bcc,message_date_time,message_subject,message_content,message_unread,account_id,folder_id) values
("raca@gmail.com","zure@gmail.com",null,null,'2019-01-11 00:01:01',"Srecna nova godina","Srecna nova drugar",false,2,1);
insert into messages(message_from, message_to, message_cc, message_bcc,message_date_time,message_subject,message_content,message_unread,account_id,folder_id) values
("zure@gmail.com","raca@gmail.com",null,null,'2019-01-11 00:02:01',"Srecna nova godina","Hvala ndvrati kd budes mpgo obde je ludnica aaaaaaaaaaaaaaaa",false,2,1);
insert into messages(message_from, message_to, message_cc, message_bcc,message_date_time,message_subject,message_content,message_unread,account_id,folder_id) values
("rade@gmail.com","raca@gmail.com",null,null,'2019-02-11 00:01:01',"vic","gubljenje max",false,2,2);

create table photos (
 photo_id bigint auto_increment,
 photo_path varchar(50),
 contact_id bigint,
 primary key (photo_id)
);

create table rules(
	rule_id bigint auto_increment,
	rule_condition ENUM('TO', 'FROM','CC','SUBJECT'),
	rule_operation ENUM('MOVE', 'COPY','DELETE'),
	rule_value varchar(100),
	folder_id bigint,
	primary key (rule_id)
);

create table tags(
tag_id bigint auto_increment,
tag_name varchar(100),
user_id bigint,
primary key (tag_id)
);
INSERT INTO tags (tag_name,user_id) VALUES ('hitno',1);
INSERT INTO tags (tag_name,user_id) VALUES ('vazno',2);

create table message_tags(
message_id bigint,
tag_id bigint
);
INSERT INTO message_tags (message_id, tag_id) VALUES (1, 1);

create table folders(
folder_id bigint auto_increment,
folder_name varchar(100),
folder_parent_folder bigint,
account_id bigint,
primary key (folder_id)
);
INSERT INTO folders(folder_name, account_id, folder_parent_folder)VALUES('Inbox', 1, null);
INSERT INTO folders(folder_name, account_id, folder_parent_folder)VALUES('Sent', 1, null);
INSERT INTO folders(folder_name, account_id, folder_parent_folder)VALUES('Inbox', 2, null);
INSERT INTO folders(folder_name, account_id, folder_parent_folder)VALUES('Sent', 2, null);
INSERT INTO folders(folder_name, account_id, folder_parent_folder)VALUES('Inbox', 3, null);
INSERT INTO folders(folder_name, account_id, folder_parent_folder)VALUES('Sent', 3, null);
